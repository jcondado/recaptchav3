
<html>
	<head>
	    <!-- Required meta tags-->
	    <meta charset="UTF-8">
	    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	    <meta name="description" content="Colorlib Templates">
	    <meta name="author" content="Colorlib">
	    <meta name="keywords" content="Colorlib Templates">

	    <!-- Title Page-->
	    <title>Formulario de Registro</title>

	    <!-- Icons font CSS-->
	    <link href="{$root_directory_views}/vendor/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">
	    <link href="{$root_directory_views}/vendor/font-awesome-4.7/css/font-awesome.min.css" rel="stylesheet" media="all">
	    <!-- Font special for pages-->
	    <link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet">

	    <!-- Main CSS-->
	    <link href="{$root_directory_views}/css/main.css" rel="stylesheet" media="all">
	    <link href="{$root_directory_views}/css/bootstrap.min.css" rel="stylesheet" media="all">	    
	</head>
	<body>
	    <div class="page-wrapper bg-blue p-t-100 p-b-100 font-robo">
	        <div class="wrapper wrapper--w680">
	            <div class="card card-1">
	                <div class="card-heading"></div>
	                <div class="card-body">
	                    <h2 class="title">Datos de la Persona</h2>
	                    <!-- INCLUIR AQUÍ UN FORMULARIO CON NOMBRE, APELLIDOS, EMAIL, TELEFONO -->
			            {if ($registro eq 1)}
		                <div class="alert alert-success" role="alert">
		                    <button type="button" class="float-right close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button>
		                    Los datos fueron registrados correctamente! 
		                </div>
		                {/if}
		                {if ($registro eq 2)}
		                <div class="alert alert-danger" role="alert">
		                    <button type="button" class="float-right close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button>
		                    Disculpe, los datos no pudieron ser registrados!
		                </div>
		                {/if}
	                    <form method="POST" action="" method="post" class="needs-validation" novalidate>
	                        <div class="row row-space">
	                            <div class="col-6">
	                                <div class="form-row">
	                                	<label for="validationCustom01">Nombres</label>
									    <input type="text" class="form-control" id="validationCustom01" placeholder="Ingrese sus nombres" name="nombres" maxlength="100" required>
									    <div class="valid-feedback">
									        Correcto!
									    </div>	                                    
	                                </div>
	                            </div>
	                            <div class="col-6">
	                                <div class="form-row">
	                                	<label for="validationCustom02">Apellidos</label>
									    <input type="text" class="form-control" id="validationCustom02" placeholder="Ingrese sus apellidos" name="apellidos" maxlength="100" required>
									    <div class="valid-feedback">
									        Correcto!
									    </div>	                                    
	                                </div>
	                            </div>
	                        </div>
	                        <div class="row row-space">
	                            <div class="col-6">
	                                <div class="form-row">
	                                	<label for="validationCustom03">Correo</label>
									    <input type="email" class="form-control" id="validationCustom03" placeholder="Ingrese su correo" name="email" maxlength="100" required>
									    <div class="valid-feedback">
									        Correcto!
									    </div>	                                    
	                                </div>
	                            </div>
	                            <div class="col-6">
	                                <div class="form-row">
	                                	<label for="validationCustom04">Telefono</label>
									    <input type="number" class="form-control" id="validationCustom04" placeholder="Ejemplo: 04241234567" name="telefono" maxlength="100" required pattern="^\d{4}-\d{3}-\d{4}$">
									    <div class="valid-feedback">
									        Correcto!
									    </div>	                                    
	                                </div>
	                            </div>
	                        </div>
	                        <div class="row mt-4">
	                        	{$recaptcha->render()}	   
	                        	<button class="btn btn--radius btn--green" type="submit">Guardar</button>
	                    	</div>	
	                        <!-- -->
	                    </form>
	                </div>
	            </div>
	        </div>
	    </div>
		<!-- JS-->
		<script src='https://www.google.com/recaptcha/api.js'></script>
	    <script src="{$root_directory_views}/vendor/jquery/jquery.min.js"></script>		
	    <script type="text/javascript" src="{$root_directory_views}/vendor/bootstrap/bootstrap.min.js"></script>	
	    <script>	    	
			(function() {
			  'use strict';
			  window.addEventListener('load', function() {
			    var forms = document.getElementsByClassName('needs-validation');
			    var validation = Array.prototype.filter.call(forms, function(form) {
			      form.addEventListener('submit', function(event) {
			        if (form.checkValidity() === false) {
			          event.preventDefault();
			          event.stopPropagation();
			        }
			        form.classList.add('was-validated');
			      }, false);
			    });
			  }, false);
			})();
		</script>
	</body>
</html>