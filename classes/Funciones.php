<?php

require_once 'vendor/autoload.php';

class Funciones
{	
	public function obtenerIp(){
	    if (getHostByName(getHostName())) {
	        return getHostByName(getHostName());
	    }else{
	        return $_SERVER["REMOTE_ADDR"];
	    }
	}
}

